The data sets used in the experiments section of the VAT+Fuzzy ART paper are available at:

UCI machine learning repository: http://archive.ics.uci.edu/ml
Fundamental Clustering Problems Suite (FCPS): https://www.uni-marburg.de/fb12/arbeitsgruppen/datenbionik/data?language_sync=1
Datasets package: https://www.researchgate.net/publication/239525861_Datasets_package
Clustering basic benchmark: http://cs.uef.fi/sipu/datasets

The "main_example.m" file contains an example of usage of the code provided.